const { describe, expect, test } = require('@jest/globals');

const { samples, mockSupporter } = require('../lib/test-utils');
const core = require('../lib/core');
const { processMINED } = require('../lib/receiver');

jest.setTimeout(10 * 1000);

describe('Testing core.eventFlusher', () => {

  const FIXED_SAMPLE = 3;

  test(`Loading a previously received event`, async () => {

    const mined = processMINED(mockSupporter, samples[FIXED_SAMPLE]);

    // because mobilizon only show events in the future, and 
    // the sample have been collected in the past, let's hack the dates
    mined.dates.start.setDate(mined.dates.start.getDate() + 365);
    if(mined.dates.end)
      mined.dates.end.setDate(mined.dates.end.getDate() + 380);

    try {
      const mobiposted = await core.flushEvents(mined,
        mockSupporter, samples[FIXED_SAMPLE].settings);

      expect(mobiposted).toHaveProperty('__typename', 'Event');
    } catch(error) {
      console.log(`[error]: ${error.message}`);
      expect(error.message).toBe("check out this error"); 
    }
  });

});

