const _ = require('lodash');
const fs = require('fs');

try {
    const { test } = require('@jest/globals');
    test.skip('skip', () => {});
} catch(error) {
    /* this error would be triggered when loaded outside a test,
     * but this inclusion is necessary because this file is under a test */
}

function verifyCleaning(fname) {
    console.log(`processing and cleaning ${fname}`);
    const data = fs.readFileSync(fname, 'utf-8');
    const json = JSON.parse(data);
    delete json['settings'];
    fs.writeFileSync(fname, JSON.stringify(json, null, 1), 'utf-8');
}

const files = fs.readdirSync('.');
_.each(files, function(f) {
    if(_.endsWith(f, '.json') && f.match(/^[0-9].*/))
        verifyCleaning(f);
});

console.log("Files sanitized (if necessary)");
