The `*.json` files here are collected in this way:

```
 function processHTMLs(supporter, received) {
 
+    const fs = require('fs');
+    const pathfname = `/tmp/${_.random(1, 0xfff)}.json`;
+    fs.writeFileSync(pathfname, JSON.stringify(received, undefined, 2), 'utf-8');
+    debug("Saved for debug %s", pathfname);
+

```

and they are meant to feed the unit testing
