/* This test verify which HTML is produced from the `recursiveReassembly` */
const { describe, expect, test } = require('@jest/globals');
const _ = require('lodash');

const { samples } = require('../lib/test-utils');
const { recursiveReassembly } = require('../lib/receiver');

describe('recursiveReassembly HTML checking', () => {

  test(`recursiveReassembly should produce text`, () => {
    _.each(samples, function(payload) {
      const description = recursiveReassembly(payload.textnest);
      // console.log(description);
      const seeless = description.match(/See\ less\<\/p\>/);
      expect(seeless).toBeNull();
    });
  });

});
