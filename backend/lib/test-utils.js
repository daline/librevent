const fs = require('fs');
const _ = require('lodash');
const path = require('path');

function loadSampleFile(fname) {
  const content = fs.readFileSync(
    path.join('tests', '_payloads', fname),
    'utf-8');
  return JSON.parse(content);
}

const samples = _.map([
  "100.json",
  "1012.json",
  "1063.json",
  "1089.json",
  "1104.json",
  "1122.json",
  "1173.json",
  "1188.json",
  "1196.json",
  "1211.json",
  "1218.json",
  "1248.json",
  "1257.json",
  "1347.json",
  "1409.json",
  "1446.json",
  "1481.json",
  "1501.json",
  "1510.json",
  "1565.json",
  "157.json",
  "1613.json",
  "163.json",
  "1655.json",
  "1687.json",
  "1784.json",
  "1791.json",
  "1796.json",
  "1814.json",
  "1839.json",
  "1892.json",
  "1910.json",
  "1927.json",
  "1931.json",
  "1949.json",
  "1967.json",
  "1983.json",
  "85.json",
  "918.json",
  "920.json",
  "930.json",
  "939.json",
  "947.json",
  "960.json",
  "976.json",
], loadSampleFile);

const mockSupporter = {
  publicKey: "mocKey_publicKey"
};

module.exports = {
  loadSampleFile,
  samples,
  mockSupporter
}
