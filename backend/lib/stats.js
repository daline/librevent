const _ = require('lodash');
const debug = require('debug')('lib:stats');
const moment = require('moment');

const mongo = require('./mongo');

async function calculate(mongoc, collectionName, timeVar) {

    const timeoff = 30;
    const startDate = moment()
        .subtract(timeoff, 'day')
        .startOf('day')
        .toISOString();

    debug("Querying %s by [%s] since %s", collectionName, timeVar, startDate);

    const queryId = { 
        year: { $year: `$${timeVar}` },
        month: { $month: `$${timeVar}` },
        day: { $dayOfMonth: `$${timeVar}` }
    };

    const data = await mongo.aggregate(mongoc, collectionName, [
        { $match: _.set({}, timeVar, { "$gte": new Date(startDate)})},
        { $group: {
            _id: queryId,
            count: { $sum: 1 }
        }}
    ]);
    /* data looks like this:               [{
        "_id":{"year":2022,"month":12,"day":23},
        "count":1
    },{
        "_id":{"year":2022,"month":12,"day":21},
        "count":1
    }]                    so now get's cleaned */

    const cleaned = _.map(data, function(e) {
        /* month and day need constant size, because
           in the next function they need to be matched */
        const month = (e._id.month < 10) ?
            `0${e._id.month}`: e._id.month;
        const day = (e._id.day < 10) ?
            `0${e._id.day}`: e._id.day;
        return {
            day: `${e._id.year}-${month}-${day}`,
            count: e.count
        }
    });
    debug("Found %d days with measurable activity", cleaned.length);

    /* but to make the rendering easier, we provide also the
     * days where the value is 0 */
    _.times(timeoff, function(daysago) {
        const checkableDate = moment()
            .subtract(daysago, 'day')
            .startOf('day')
            .format("YYYY-MM-DD");
        if(!_.find(cleaned, { day: checkableDate })) {
            cleaned.push({
                day: checkableDate,
                count: 0
            })
        }
    });

    return cleaned;
}

async function errors(req, res) {
    /* this API returns the last 20 errors in detail */
    const mongoc = await mongo.clientConnect();
    const errorList = await mongo
       .readLimit(mongoc, "errors", {}, { when: -1}, 20, 0);
    await mongoc.close();
    debug("Retrieved %d errors", errorList.length);
    return { json: errorList };
}

async function produce(req, res) {
    /* this API should be cached and only every 10 minutes a new 
     * computation should allow to occour. This API returns counters 
     * that describes the usage */
    const mongoc = await mongo.clientConnect();

    /* this piece of code do not use nconf.get('schema') like others,
     * as the variable with the date is tight to the collection version */
    const collectionStats = {
        "supporters": "keyTime",
        "activities": "when",
        "htmls": "savingTime",
        "errors": "when",
        "metadata": "savingTime",
    };

    const ret = [];
    for(const collectionName of _.keys(collectionStats) ) {
        const timeVar = _.get(collectionStats, collectionName);
        const stats = await calculate(mongoc, collectionName, timeVar);
        ret.push({
            collectionName,
            stats,
        });
    }

    await mongoc.close()
    return { json: ret };
}

module.exports = {
    produce,
    errors,
};
