/*
 * this library module represent the 'core' of librevent 0.3.x, as it is the
 * interface between the backend and the mobilizon instance
 * is uses the module @_vecna/mobilizon-poster to handle this */

const _ = require('lodash');
const debug = require('debug')('lib:core');
const mobi = require('@_vecna/mobilizon-poster').lib;


async function flushEvents(mined, supporter, config) {
    // this function might be bigly optimized, but, at the moment
    // it takes a blob of data from the extension, among them
    // the authentication material.

    // debug('mined: %j config %j supporter %j', mined, config, supporter);

    const { login, password, mobilizon: mobisrvapi } = config;
    // by following the small documentation here
    // https://www.npmjs.com/package/@_vecna/mobilizon-poster

    let token, userInfo;
    try {
        token = await mobi.login.perform(login, password, mobisrvapi);
        const response = await mobi.login.getInfo(token, mobisrvapi);
        userInfo = response.identities[0];
        // userInfo has this content
        /* [{
         *   "__typename": "Person",
         *   "avatar": null,
         *   "id": "12",
         *   "name": "experiment",
         *   "preferredUsername": "experiment",
         *   "type": "PERSON"
         * }]
         */
    } catch(error) {
        debug("Error in connecting to mobilizon with third via mobilizon-poster package!");
        throw error;
    }

    const location = await testLocations(mined.address, mobisrvapi, token);

    const eventvars = {
        ...mined.dates, // start (optionally also) end
        title: mined.title,
        description: mined.description,
        url: mined.href.replace(/\?.*/, ''),
        address: mined.address,
        location
    };

    /* this is the internal userId used by mobi */
    eventvars.organizerActorId = userInfo.id;
    eventvars.token = token;

    try {
        const results = await mobi.createEvent.postToMobilizon(eventvars);
        debug("createEvent [%s] successful!", eventvars.title);
        return results;
    } catch(error) {
        debug("Failure in createEvent call: %s", error.message);
        throw error;
    }
}

async function testLocations(addressl, mobisrvapi, token) {

    debug("Trying to resolve location in %O", addressl);

    const locations = [];
    for (const address of addressl) {
        try {
            const r = await mobi.location.queryLocation(
                address,
                localString=null,
                apiEndpoint=mobisrvapi,
                token
            );
            locations.push(r);
        } catch(error) {
            debug("Not found a location for [%s]", address);
        }
    }
    if(_.compact(locations).length > 1)
        debug("Unusual condition where both of the label worked %O", addressl);

    return _.first(_.compact(locations));
}

module.exports = {
    flushEvents
};
