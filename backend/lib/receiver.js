const _ = require('lodash');
const moment = require('moment');
const debug = require('debug')('lib:receiver');
const activities = require('debug')('activities:receiver');
const nconf = require('nconf');

const core = require('./core');
const mongo = require('./mongo');
const utils = require('./utils');

/* note, a few functions in this file are outdated and should be deleted,
 + a discrete knowledge of lodash would help you for good */

function recursiveReassembly(objtree) {
    /* this object is specular to the text fetched 
     * from the source, it is initially produced in
     * extension/src/hasher.js file recursiveLinksDigging */
   
    /* this is the return variable, it accumulates piece of HTML */
    let htmlacc = "";
    /* if you dump the content of objtree with console.log 
     * you might see there are a lot of text we don't need,
     * this boolean 'stillIgnore' is true till the string: */
    const ignoreUntil = "Anyone on or off Facebook";
    let stillIgnore = true;
    let finish = false;

    /* this function need to exclude the pieces of text 
     * that we don't need */
    function handleHref(ahrefo) {
        if(stillIgnore)
            return;
        
        htmlacc += `<a target=_blank href="${ahrefo.href}">${ahrefo.text}</a>`;
    }

    function handleText(texto) {
        if(stillIgnore && texto.text === ignoreUntil) {
            stillIgnore = false;
            // because otherwise we add 'ignoreUntil' in text 
            return;
        }

        if(stillIgnore)
            return;

        if(texto.text === "See less") {
            finish = true;
            return;
        }

        htmlacc += `<p dir="auto">${texto.text}</p>`;
    }

    /* this function do recursion and understand if 
     * is dealing with text, links, or objects */
    function objectDigging(obj) {

        /* finish is set when a 'see less' is seen, so we ensure
         * the spurious information at the bottom are cutoff, links or text */
        if(finish)
            return;

        if(obj instanceof Object) {
            /* objects might be { text: "blah" }, or
             * { href: "...", text: "blah" } so we check
             * before the href to handle the link differently */
            if(obj.href)
                handleHref(obj);
            else if(obj.text)
                handleText(obj);
        }
        if(obj instanceof Array) {
            _.each(obj, objectDigging);
        }
    }

    _.each(objtree, objectDigging);

    return htmlacc;
}

function bruteformat(datestr) {
    // this function needs a lot of improvement and testing

    const formats = [
        "dddd MMMM D YYYY",
        "MMM D YYYY HH",
        "MMM D YYYY"
    ];

    return _.reduce(formats, function(memo, fmt) {
        if(memo)
            return memo;

        let att = moment(datestr, fmt);

        // enble this if you're doing unit testing of these functions
        /* if(att.isValid())
            console.log("Success with ", fmt, "for", datestr); */
        return att.isValid() ? att : null;
    }, null)
};

function dissectDates(inputs) {
    // cases considered in v0.3.2:

    // JAN 28, 2023 AT 7:00 AM – JAN 29, 2023 AT 11:00 AM UTC+07
    // "MONDAY, NOVEMBER 21, 2022 AT 3:00 PM – 4:00 PM UTC-03",
    // "THURSDAY, NOVEMBER 17, 2022 AT 5:30 PM – 8:30 PM EST",
    // "NOV 17 AT 7:15 PM – NOV 19 AT 8:30 PM UTC-05",

    // this function needs a lot of improvement and testing

    const timezone = inputs.match(/[AP]M\ [A-Z]{3,4}[\-+]{0,1}[0-9]{0,2}$/);
    if(!timezone) {
        // this is not a fatal error
        debug("Unable to parse timezone? (%s)", inputs);
    } else {
        const tzstr = timezone[0];
        inputs = inputs.substring(0, timezone.index);
    }

    const regexps = {
        'simple': /[A-Z]{5,10},\ [A-Z]{3,10}\ [0-9]{1,2},/
    };

    const splitchar = '–';
    const chunks = inputs.split(splitchar);

    const startm = bruteformat(chunks[0]);

    if(!startm) {
        debug("[dissectDates failure] %s", inputs);
        throw new Error("Unable to parse DateTime");
    }

    let end = bruteformat(chunks[1]);
    // enble this if you're doing unit testing of these functions
    // console.log(start, end, chunks[1]);

    /* end can be 'null' but this is giving some bug, so this gets hacked
     * this way now */
    if(!end || !end.isValid()) {
        debug("Forcing 'end' to the end of the day");
        end = startm.endOf('day');
    }

    if(end.isBefore(startm)) {
        debug("Time travel problem!");
        end = startm.endOf('day');
    }

    const retval = { 
        start: new Date(startm),
        end: new Date(end),
        tzinfo: timezone ? timezone[0] : null
    };

    debug("dissectDates from [%s] start: %s end %s", inputs,
        startm.format("YYYY-MM-DD HH:mm"), end.format("YYYY-MM-DD HH:mm")
    );

    return retval;
}

function processMINED(supporter, received) {

    const id = utils.hash({
        href: received.href,
        size: received.size,
        publicKey: supporter.publicKey,
        randomUUID: received.randomUUID,
    });

    const mined = _.pick(received, [
        'details',  // information mined in the extension, the blocks with RED border
        'clientTime',
        'title',
        'href',     // facebook event URL
        'update'    // this is an incremental number in the case the same page in the browser has been sent more than once
    ]);
    mined.id = id;
    // the two dates should not be String but Date
    mined.clientTime = new Date(mined.clientTime);
    mined.savingTime = new Date();

    // fundamental to keep track of the data liberator
    mined.publicKey = supporter.publicKey;

    mined.address = received.place.desc; // this is a list
    mined.addressLink = received.place.href;
    try {
        mined.dates = dissectDates(received.date);
    } catch(error) {
        activities("Error in dissectDates (%s): %s", received.date, error.message);
        mined.dates = {
            start: new Date(
                moment().add(1, 'day').startOf('day').toISOString()
            ),
            end: new Date(
                moment().add(1, 'day').endOf('day').toISOString()
            ),
            tzinfo: null,
        };
        debug("Attributing default dates as %j", mined.dates);
    }

    // reminder: mobilizon might support HTML too!
    mined.description = recursiveReassembly(received.textnest);
    return mined;
}

function processHTMLs(supporter, received) {

    /* optional code used to dump tests/_payloads/*.json
    const fs = require('fs');
    const pathfname = `/tmp/${_.random(1, 0xfff)}.json`;
    fs.writeFileSync(pathfname, JSON.stringify(received, undefined, 2), 'utf-8');
    debug("Saved for debug %s", pathfname); */

    const id = utils.hash({href: received.href,
        size: received.size,
        publicKey: supporter.publicKey,
        randomUUID: received.randomUUID,
    });
    /* different URL type causes different type */
    const linktype = received.href.match(/\/events\/(\d+)/) ? "event" : "previews";
    const evelif = {
        id,
        linktype,
        href: received.href,
        html: received.element,
        update: received.update,
        publicKey: supporter.publicKey,
        clientTime: new Date(received.clientTime),
        savingTime: new Date(),
    };
    return evelif;
}

async function createSupporter(mongoc, publicKey) {
    const supporter = {
        publicKey,
        keyTime: new Date(),
        lastActivity: new Date(),
    };                                                                                               
    supporter.pseudo = utils.pseudonymizeUser(publicKey);
    debug("Creating supporter %s %s", supporter.pseudo, publicKey);
    await mongo.writeOne(mongoc, nconf.get('schema').supporters, supporter);
    return supporter;
}

const requiredHeaders =  {
    'x-trex-publickey': 'publickey',
    'x-trex-signature': 'signature'
};

async function processEvents(req) {

    /* activities logger */
    const mongoc = await mongo.clientConnect();
    await mongo.writeOne(mongoc, nconf.get('schema').activities, {
        when: new Date(),
        bodySize: JSON.stringify(req.body).length
    });
    await mongoc.close();
    /* not the most efficient what to open and close a mongod connection but ..*/

    const headers = _.reduce(requiredHeaders, function(memo, destk, hdrnamek) {
        let received = _.get(req, 'headers');
        let exists = _.get(received, hdrnamek);
        if(exists) {
            _.set(memo, destk, exists);
            return memo;
        } else {
            debug("headers parsing error missing: %s - %j", hdrnamek, received);
            throw new Error("Missing header "+ hdrnamek);
        }
    }, {});

    /* first: verify signature integrity */
    let message = req.body;
    if (req.headers['content-type'] === 'application/json') {
        message = JSON.stringify(message)
    }
    if (!utils.verifyRequestSignature(headers.publickey, headers.signature, message)) {
        debug("event ignored: invalid signature, body of %d bytes, headers: %j",
            _.size(message), req.headers);
        throw new Error("Invalid signature");
    }
    /* crypto verification complete */

    const rv = await importOrFail(_.first(req.body), headers);
    return { json: rv };
};

async function importOrFail(payload, headers) {

    const beginning = moment();
    const mongoc = await mongo.clientConnect();
    let rv = null;

    let supporter = await mongo.readOne(mongoc, nconf.get('schema').supporters, {
        publicKey: headers.publickey
    });

    if(!_.size(supporter)) {
        supporter = await createSupporter(mongoc, headers.publickey);
        activities("Created a new supporter %s (%O)",
            supporter.pseudo, payload?.settings);
    }

    debug(" * Supporter %s [active on %s] last activity %s (%s ago)",
        supporter.pseudo, moment().format("HH:mm"),
        moment(supporter.lastActivity).format("HH:mm DD/MM"),
        moment.duration(moment.utc()-moment(supporter.lastActivity)).humanize() );

    try {

        /* second thing: check if the settings are complete */
        const m = payload?.settings?.mobilizon ?? "";
        const l = payload?.settings?.login ?? "";
        const p = payload?.settings?.password ?? "";
        if(!p.length || !l.length || !m.length) {
            activities("Incomplete configuration: %O writing an error for %s",
                payload?.settings, supporter.pseudo);
            throw new Error("Missing configuration");
        }

        const html = processHTMLs(supporter, payload);
        await mongo
            .writeOne(mongoc, nconf.get('schema').htmls, html);

        activities("Processing the event from %s", payload.href);

        const mined = processMINED(supporter, payload);
        /* supporter and config are in two different collections, both handled in 'core', 
        * where the authentication token of mobilizon is fetched */

        const mobiposted = await core.flushEvents(mined, supporter, payload.settings);

        if(mobiposted['__typename'] === 'Event')
            mined.posted = mobiposted;
        else
            mined.failure = mobiposted;

        _.set(supporter, 'lastActivity', new Date());
        /* save in supporter collection the most recent settings used */
        supporter.config = payload.settings;
        // debug("Support updated as %O", supporter);

        await mongo
            .updateOne(mongoc, nconf.get('schema').supporters, {
                publicKey: supporter.publicKey
            }, supporter);

        // TODO remind the `posted` collection has not index nor yet clear structure
        await mongo.writeOne(mongoc, nconf.get('schema').metadata, mined);

        const conclusion = moment();

        activities("Written event post [%s] (%s) supporter %s, took %s",
            mined?.posted?.url ? "SUCESSFUL" : "FAILURE",
            mined?.posted?.url ? mined.posted.url : JSON.stringify(mined.failure),
            supporter.pseudo,
            moment.duration(conclusion - beginning).humanize());

        rv = {
            status: mined?.posted?.url ? "OK" : "error",
            url: mined?.posted?.url,
            seconds: moment.duration(conclusion - beginning).asSeconds(),
        };

    } catch(error) {
        activities("import fail & error saved: [%s]", error.message);
        await mongo.writeOne(mongoc, nconf.get('schema').errors, {
            message: error.message,
            publicKey: supporter.publicKey,
            when: new Date()
        });
        const conclusion = moment();
        rv = {
            status: "error",
            message: error.message,
            seconds: moment.duration(conclusion - beginning).asSeconds(),
        }
    }

    await mongoc.close();
    return rv;
}

async function returnEvent(req) {

    const eventId = _.parseInt(req.params.eventId);
    const mongoc = await mongo.clientConnect();
    const event = await mongo.readLimit(mongoc, nconf.get('schema').metadata, {
        eventId
    }, { savingTime: -1}, 30, 0);
    debug("Fetched %d event(s) for %d", _.size(event), eventId);
    await mongoc.close();
    const ready = _.map(event, function(e) { return _.omit(e, ['_id', 'publicKey']) });
    return { json: ready };
};

async function personalContribs(req) {
    // personal Contribution, is the API accessed only via knowledge of the personal 
    // secret (which technical is a publicKey so a better authentication mechanism can be 
    // implemented, for example what has been done in fbtrex and/or a challenge-response mechanism

    // this API returns the `supporter` entry with the `supporter.config` settings,
    // and returns also the list of the last 100 liberated events. this is an arbitrary 
    // number and pagination isn't yet implemented. 

    const publicKey = req.params.publicKey;
    debug("Requested personalContribs by %s", publicKey);

    const mongoc = await mongo.clientConnect();
    const metadata = await mongo.readLimit(mongoc, nconf.get('schema').metadata, {
        publicKey 
    }, { savingTime: -1}, 100, 0);
    const errors = await mongo.readLimit(mongoc, nconf.get('schema').errors, {
        publicKey 
    }, { when: -1}, 100, 0);
    const supporter = await mongo.readOne(mongoc, nconf.get('schema').supporters, {publicKey})

    if(!supporter) {
        debug("User not found, returning an error message");
        return {
            json: { error: true, message: "User not found. Liberate an event and it will be created!" }
        }
    }

    supporter.created = moment.duration(new Date(supporter.keyTime) - new Date()).humanize() + " ago";
    supporter.accessed = moment.duration(new Date(supporter.lastActivity) - new Date()).humanize() + " ago";

    _.each(metadata, function(m) {
        m.when = moment.duration(new Date(m.savingTime) - new Date()).humanize() + " ago";
        _.unset(m, 'details');
        m.eventTime = m.dates?.start ? moment(m.dates.start).format("YYYY-MM-DD") : "???";
    });

    debug("Fetched for (%s) %d metadata and %d errors",
        supporter.pseudo, metadata.length, errors.length);

    await mongoc.close();
    return { 
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        json: { metadata, supporter, errors }
    };
}

module.exports = {
    processEvents,
    processMINED,
    returnEvent,
    dissectDates,
    recursiveReassembly,
    personalContribs,
};
