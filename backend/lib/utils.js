const _ = require('lodash');
const crypto = require('crypto');
const foodWords = require('food-words');
const bs58 = require('bs58');
const nacl = require('tweetnacl');
const nconf = require('nconf');

function hash(obj, fields) {
    // please remind self: NEVER CHANGE THIS
    if(_.isUndefined(fields))
        fields = _.keys(obj);
    const plaincnt = fields.reduce(function(memo, fname) {
        return memo += fname + "∴" + _.get(obj, fname, '…miss!') + ",";
    }, "");
    // debug("Hashing of Object %s", plaincnt);
    sha1sum = crypto.createHash('sha1');
    sha1sum.update(plaincnt);
    return sha1sum.digest('hex');
};

function smallhash(obj) {
    return hash(obj).substr(0, 6);
};

function parseIntNconf(name, def) {
    let value = nconf.get(name) ? nconf.get(name) : def;
    return _.parseInt(value);
}

function pseudonymizeUser(userId) {
    return pseudonymize(`user${userId}`, 3);
}

function pseudonymize(piistr, numberOf) {
    const inputs = _.times(numberOf, function(i) {
        return _.reduce(i + piistr, function(memo, acharacter) {
            var x = memo * acharacter.charCodeAt(0);
            memo += ( x / 23 );
            return memo;
        }, 1);
    });
    const size = _.size(foodWords);
    const ret = _.map(inputs, function(pseudornumber) {
        return _.nth(foodWords, (_.round(pseudornumber) % size));
    });
    return _.join(ret, '-');
};

function number2Food(number) {
    const size = _.size(foodWords);
    const first = _.nth(foodWords, (number % size));
    const second = _.nth(foodWords, ( (number * 2) % size));
    return [ first, second ];
};

function string2Food(text) {
    const number = _.reduce(_.split(text), function(memo, c) {
        if(c.charCodeAt() < 60)
           return memo * (c.charCodeAt() + 1);

        return c.charCodeAt() + memo;
    }, 1);
    return number2Food(number);
};

function stringToArray (s) {
    // Credits: https://github.com/dchest/tweetnacl-util-js
    var d = unescape(encodeURIComponent(s));
    var b = new Uint8Array(d.length);

    for (var i = 0; i < d.length; i++) {
        b[i] = d.charCodeAt(i);
    }
    return b;
}

function encodeToBase58 (s) {
    return bs58.encode(s);
}

function decodeFromBase58 (s) {
    return new Uint8Array(bs58.decode(s));
}

function verifyRequestSignature(publicKey, signature, message) {

    // FIXME: apparently with Express 4 the body is a streamed buffer,
    // and I don't want to dig in that now. My "There I Fix It" solution
    // is to dump the json of the body in a string, and use that to verify
    // the signature.
    //
    //   WARNING!!!
    //   This works good when the client sending the data is in JavaScript
    //   as well, since key order is given by the insertion order.

    return nacl.sign.detached.verify(
        stringToArray(message),
        decodeFromBase58(signature),
        decodeFromBase58(publicKey));
};

module.exports = {
    hash,
    smallhash,
    parseIntNconf,
    pseudonymizeUser,
    pseudonymize,
    number2Food,
    string2Food,
    stringToArray,
    encodeToBase58,
    decodeFromBase58,
    verifyRequestSignature,
};
