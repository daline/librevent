## [𝕃𝕀𝔹ℝ𝔼𝕍𝔼ℕ𝕋𝕊](https://libr.events)

This repository contains:

1. is the HTML/markedown and template used by Hugo to generate https://libr.events
2. the backend code running on libr.events
3. the browser exension code

### Site

```
git submodule update --init 
cd site
hugo -D server
```
Then connect to localhost:1313

Content is in the 'content' directory; Remind the [typography](https://libr.events/examples/list/).

`# git submodule add https://github.com/queensferryme/hugo-theme-texify.git themes/hugo-theme-texify`

### Backend and Extension

You need: `node` and `mongodb` running on your computer.

```
sh ./buildall.sh
```

This would build `extension/build/` which can be loaded into your browser, and in development mode would send data to `localhost:13000`. To spin the server, you should run `backend/collector.js` in this way:

```
~/D/librevent/backend$ npm run watch

> librevent-backend@0.3.2 watch
> key=fuffa DEBUG=librevent:*,mobi:*,lib:* nodemon collector.js --config ./nodemon.json

[nodemon] 2.0.20
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node collector.js`
 ઉ nconf loaded, using ./settings.json  — mongoDb: librevent
  lib:mongodb Initializing mongoUri as mongodb://localhost:27017/librevent +0ms
  librevent:server Binded sucessfully port 13000 +6ms
 MongoDb connection works!
```

Then:

1. load the extension in developer mode
2. access to a facebook event [like this](https://www.facebook.com/events/776764820228294), you should see the page displaying a rocket button (🚀). 
3. by moving your mouse over it, you'll get a message about the status and what you can do
4. if the background is green: wow! click it, and liberate your first event
5. click on the extension icon to access a _personal page_

Check on https://libr.events/api/version and http://localhost:13000/api/version to verify if you're aligned!

---

# about `Data Liberation Network`

Data Liberation is the action of copying material out of corporate platform so peoole can find more content into the fediverse. It is a support on the migration between the centralized point of failure and the federated counterpart.

It should not be seen as a simple and automated action, as it is not enough to scrape material and post it somewhere else:

1. it would likely downgrade the quality of the federated experience if the copy is mindless
2. woud not be an incentive for people to leave corporate platforms.


### Why matter ?

Facebook empire based on owning data and decide how to serve them. These "data" is provided by people. People coalize to rivindicate their decisional power. Fediverse replace facebook. Librevents copy events from Facebook and repost them on Fediverse. 

Critical mass is the reason why Facebook remain dominan despite none of their records justify this. Librevent claim is:

### By liberating events from Facebook to Fediverse we might help migration on federated platform.

And that's all we're testing here! If you want to get more grasp of the actual impact please check out [libr.events/about](https://libr.events/about), don't miss also [libr.events/privacy](https://libr.events/privacy) and please consider version **0.3.x as an ALPHA release**.

### Librevent focus in building a [bridge of support for people](https://libr.events/post/who-are-we-talking-to/#3-the-advocates) that wants to leave Facebook events.

It include this software, the federated replacement for event management [mobilizon](https://joinmobilizon.org), and also a nodejs package developed named [mobilizon-poster](https://github.com/vecna/mobilizon-poster).

### What's interoperability ? 

Interoperability (_from wikipedia_) is a characteristic of a product or system, whose interfaces are completely understood, to work with other products or systems, at present or in the future, in either implementation or access, without any restrictions. While the term was initially defined for information technology or systems engineering services to allow for information exchange, a broader definition takes into account social, political, and organizational factors that impact system to system performance. Hence interoperability involves the task of building coherent services for users when the individual components are technically different and managed by different organizations.

#### Other repositories directly referenced in this site

* [librevent](https://github.com/tracking-exposed/librevent)
* [mobilizon-poster](https://github.com/vecna/mobilizon-poster)
