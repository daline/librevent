#!/usr/bin/env bash

rm -rf ./dist
NODE_ENV=production node_modules/.bin/webpack -p

echo "Manually removing 'localhost' from the manifest.json"
grep -v localhost manifest.json | grep -v 127\.0 | sed -es/-dev// > ./dist/manifest.json

cp src/popup/* ./dist
cp icons/*.png ./dist
cd ./dist
zip extension.zip * 

