import React from 'react';
import config from '../../../config';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import List from '@material-ui/core/List';

function ListItemLink (props) {
    return <ListItem component="a" {...props} />;
}

class InfoBox extends React.Component {
    render () {
        const personalLink = config.WEB_ROOT + '/personal/#' + this.props.publicKey;

        return (
          <List component="nav" aria-label="controls links files">

            <ListItem button>
              <ListItemIcon>
                <AccountTreeIcon fontSize='large' color='primary' />
              </ListItemIcon>
              <ListItemLink href={personalLink} target="_blank">Access the Personal page</ListItemLink>
            </ListItem>

          </List>
        );
    }
};

export default InfoBox;
