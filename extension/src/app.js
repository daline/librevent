// Import other utils to handle the DOM and scrape data.
import _ from 'lodash';

import { createPanel, dropMessage } from './panel';
import config from './config';
import hub from './hub';
import { registerHandlers } from './handlers/index';

/* the included files here are slightly misleading: the 'hasher' is doing parsing,
 * and the parser was an experiment for a different kind of parser */
import { mineEvent } from './parser';
import { investigate, looks, dates, title, description, getPlace } from './hasher';

// bo is the browser object, in chrome is named 'chrome', in firefox is 'browser'
const bo = chrome || browser;

function getRandomBlock () {
  return Math.random().toString(36).substring(2, 13) +
    Math.random().toString(36).substring(2, 13);
}

// variable used to spot differences when there is a refresh or an url change
let randomUUID = 'INIT|' + getRandomBlock();

// Frequency of url change and decision
const hrefPERIODICmsCHECK = 2100;
let lastURL = null;
const FB_PAGE_SELECTOR = '[role="main"]';
const cacheSize = {
  seen: 0,
  sentTimes: 0
};

// config represent the default settings, the next
// variable is extended by the information retrieved from the popup
// and it is initalized in boot
let currentConfig = null;

// Boot the user script. This is the first function called.
// Everything starts from here.
function boot () {
  // this get executed only on facebook.com
  console.log(`Librevents extension hardcoded settings: ${JSON.stringify(config, undefined, 2)}`);

  // Register all the event handlers.
  // An event handler is a piece of code responsible for a specific task.
  // You can learn more in the [`./handlers`](./handlers/index.html) directory.
  registerHandlers(hub);

  // Lookup the current user and decide what to do.
  localLookup(response => {
    // these parameters are loaded from localstorage
    console.log(`Librevents local settings: ${JSON.stringify(response, undefined, 2)}`);

    currentConfig = { ...config, ...response };
    console.log(`mixed settings: ${JSON.stringify(currentConfig, undefined, 2)}`);

    if (currentConfig.active !== true) {
      console.log('Librevents seems disabled, you should enabled it via popup');
      return;
    }

    initializeBlinks();
    window.setInterval(hrefUpdateMonitor, hrefPERIODICmsCHECK);
    flush();
  });
}

function acceptableHref (pathname) {
 /* the events path are five:
  * https://www.facebook.com/events?source=46&action_history=null
  * https://www.facebook.com/events/440531403865204/?acontext=%7B%22even
  * + page event + pg/page event
  *
  * but now we support only direct event page!
  *  */
  return !!pathname.match(/^\/events\/(\d+)/);
}

function meaningfulCachedDifference (elem) {
  const ts = elem.outerHTML.length;
  const fivepercentless = (ts / 20) * 19;
  let retval = false;

  cacheSize.sentTimes++;

  if (ts < 4000) {
    console.debug('Event triggered too early, current HTML is way too small to be collected');
  } else if (cacheSize.seen < fivepercentless) {
    console.debug('Observed a (meaningful) size update from', cacheSize.seen,
      'to', ts, 'page seen', cacheSize.seenTimes, 'times');
    retval = true;
  }
  /* else: the increase was lesser than 5% so it is ignored */

  cacheSize.seen = ts;
  return retval;
}

function cleanCache () {
  cacheSize.seen = 0;
  cacheSize.sentTimes = 0;
}

function hrefUpdateMonitor () {
  if (!acceptableHref(window.location.pathname)) {
    console.debug(window.location.pathname, 'Ignored pathname as only events are considered');
    currentPhase = null;
    return;
  }
  // check if the main element is present: this might also causes a few race
  // condition, in the case of slow network. can it be? TO BE TESTED.
  const elem = document.querySelector(FB_PAGE_SELECTOR);
  if (!elem) {
    console.log(
      'Unexpected: not found selector',
      FB_PAGE_SELECTOR,
      'in',
      window.location.href,
    );
    return;
  }

  // if is different from the previous, is the first time observed
  const diff = window.location.href !== lastURL;
  // regardless of diff, update the current one as also the last one
  lastURL = window.location.href;

  // continue the step, if new, clean cache
  if (diff) {
    cleanCache();
    randomUUID = getRandomBlock().substring(0, 28);
  }

  if (!meaningfulCachedDifference(elem)) {
    return;
  }

  console.log('Resetting phase as first');
  currentPhase = 'first';

  runPageAnalysis();

  return;

  /* the following code was meant for the previous approach, not useful anymore
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  let metadata = {};
  try {
    metadata = mineEvent(elem);
    console.log('Event spotted and mined successfully', metadata);
  } catch (error) {
    console.log('Event spotted but error in mining', error.message);
    metadata.error = error.message;
  }
  hub.event('newContent', {
    metadata,
    element: elem.outerHTML,
    href: window.location.href,
    when: Date(),
    update: cacheSize.sentTimes,
    randomUUID
  });
  */
}

// The function `localLookup` communicates with the **action pages**
// to get information about the current user from the browser storage
// (the browser storage is unreachable from a **content script**).
function localLookup (callback) {
  bo.runtime.sendMessage(
    {
      type: 'localLookup',
      payload: {
        userId: 'local' // at the moment is fixed to 'local'
      }
    },
    callback,
  );
}

// The function `remoteLookup` communicate the intention
// to the server of performing a certain test, and retrive
// the userPseudonym from the server
// this is not used atm but might be worthy
function remoteLookup (callback) {
  bo.runtime.sendMessage(
    {
      type: 'remoteLookup',
      payload: {
        // window.location.pathname.split('/')
        // Array(4) [ "", "d", "1886119869", "Soccer" ]
        // window.location.pathname.split('/')[2]
        // "1886119869"
        testId: window.location.pathname.split('/')[2]
      }
    },
    callback,
  );
}

function flush () {
  window.addEventListener('beforeunload', e => {
    hub.event('windowUnload');
  });
}

// Before booting the app, we need to update the current configuration
// with some values we can retrieve only from the `chrome`space.
bo.runtime.sendMessage({type: 'chromeConfig'}, response => {
  Object.assign(config, response);
  boot();
});
// Please debug/test this function knowing that in boot() we
// initialize currentConfig

export function displayMessageOnHover (node) {
  // this creates #panel--message so 'mouseout' event destroy it
  const messagef = document.createElement("p");
  messagef.setAttribute('class', 'panel--message');
  messagef.setAttribute('style', 'font-size:0.3em;background-color:#cdff5e;margin:3px;border:3px;padding:3px;border-radius:5px');
  if(currentPhase === 'second') {
    messagef.textContent= `Click the bordered buttons!`;
  } else if (currentPhase === 'third') {
    messagef.textContent= `Click rocket to send! 🫶`;
  } else if (currentPhase === null) {
    messagef.textContent= `Not an event page!`;
  } else {
    messagef.textContent= `phase ${currentPhase}`;
  }
  node.appendChild(messagef);

  window.setTimeout(() => {
    const messs = document.querySelectorAll(".panel--message");
    _.each(messs, function(p) { p.remove(); });
  }, 2000);

}

export function runPageAnalysis () {
  /* this function is invoked every time the user click on the left buttons.
   1) check if there are 'see more' button
   2) if there are not, switch to phase two.
      returns always the number 'see more' present, because it might also be that
      a 'see more' can't be clicked and should be handled differently:
      i.e. https://www.facebook.com/events/1203183407199422
  */

  const elem = document.querySelector(FB_PAGE_SELECTOR);
  if (!elem) return;
  if (!acceptableHref(window.location.pathname)) return;

  let buttons = -1;
  console.log('executing runPageAnalysis in phase', currentPhase);
  try {
    const buttons = seemore(elem);
    currentPhase = (!buttons.length) ? 'third' : 'second';
    const message = 'Please click to EXPAND the event details';

    _.each(buttons, function (b) {
      console.log(`Injecting for 10 seconds ${message}`);
      dropMessage(b, message, 10000);
    });

  } catch (error) {
    console.log('Error in looking for buttons to click!', error.message);
  }
  return buttons;
}

function seemore (rootnode) {

  const seemores = [
    'See more',         // English
    'Mehr anzeigen',    // German
    'Altro...',         // Italian
    'Voir plus',        // French
    'Ver más',          // Portuguese
    'Ver mais',         // Spanish
  ];

  let buttons = [];
  for (const langOpt of seemores) {
    buttons = looks(rootnode, '[role="button"]', 'textContent', langOpt);
    if (buttons.length) {
      console.log(`Matched buttons ${langOpt}`);
      break;
    }
  }

  if (!buttons.length) return [];

  console.log('Adding the red border to ', buttons.length, 'w/ content:', _.map(buttons, 'textContent'));

  _.each(buttons, function (b) {
    b.style = 'border: red 2px solid; border-radius: 5px';
    b.addEventListener('mouseout', runPageAnalysis);
  });
  return buttons;
}

function sendEvent () {
  const elem = document.querySelector(FB_PAGE_SELECTOR);
  if (!elem) return;

  try {
    const metadata = mineEvent(elem);
    console.log('Event spotted and mined successfully', metadata);
  } catch (error) {
    console.log(`Error in mineEvent: ${error.message} but doesn't matter`);
  }

  try {
    const buttons = seemore(elem);

    if (!buttons) currentPhase = 'second';

    const investiresult = investigate(elem);
    if (investiresult.length) {
      currentPhase = 'third';
    }

    const textnest = description(elem);
    const pickedTitle = title(elem);
    const date = dates(elem);
    const place = getPlace(elem);

    hub.event('newContent', {
      textnest,
      date,
      place,
      title: pickedTitle,
      details: _.map(investiresult, (i) => _.omit(i, ['node'])),
      element: elem.outerHTML,
      href: window.location.href,
      update: cacheSize.sentTimes,
      randomUUID,
      settings: _.pick(currentConfig, ['mobilizon', 'login', 'password', 'backend']),
    });
    return true;
  } catch (error) {
    console.log('Error in investigation/mining metadata:', error.message);
    return false;
  }
}

let currentPhase = null;
export function dispatchIconClick (node) {
  console.debug(`Handling a click, and currently we're in phase ${currentPhase}`);
  /* when someone click on a button, check if the event is worthy of being analyzed and trimmed */

  runPageAnalysis();

  if (!currentPhase) {
    /* for 0.8 seconds display a kind of warning message */
    dropMessage(node, 'Not an event page!', 800);
  } else if (currentPhase === 'first') {
    /* phase one is when the system look for a "See More" and look
     * if the event can be already collected */
    dropMessage(node, 'Page analysis in progress', 1800);
  } else if (currentPhase === 'second') {
    /* phase two is when by clicking on the buttons the event would be
     * liberated */
    dropMessage(node, 'Click the red boxes!', 1800);
    // const result = sendEvent();
    // console.log('Event sent and the result is', result);
  } else if (currentPhase === 'third') {
    /* phase three is when an event gets liberated */
    dropMessage(node, 'Sending...', 1800);

    // Lookup is sent to get the latest settings!
    localLookup(response => {
      currentConfig = { ...config, ...response };
      const result = sendEvent();
      console.log('Event sent and the result is', result);
    });

  } else {
    console.log(`Unmanaged condition? phase ${currentPhase}`);
  }
}

function initializeBlinks () {

  if(document.querySelector('#panel')) {
    console.log("icon already initialized...");
    return;
  }

  createPanel();
}
