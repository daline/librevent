import { dispatchIconClick, displayMessageOnHover } from './app';

export function createElement (tag, cssProp = {}, parent = document.body, id = null) {
  const element = document.createElement(tag);
  Object.entries(cssProp).forEach(([key, value]) => {
    element.style[key] = value;
  });
  if (id) {
    element.setAttribute('id', id);
  }
  parent.appendChild(element);

  return element;
}

const containerCSS = {
  position: 'fixed',
  top: '40%',
  zIndex: 9999,
  fontSize: '3em',
  padding: '5px',
  backgroundColor: '#17f250ba',
  borderRadius: '0px 20px 20px 0px',
  border: '#ecd6ff 1px solid',
  cursor: 'grab',
};

const liberatedCSS = {
  backgroundColor: '#000000e8',
};

const errorCSS = {
  backgroundColor: '#f25917ba',
};

/**
 * Create the panel
 * @param {object} Object like: {[EVENT_NAME]: {color: string}}
 */
export function createPanel () {

  const container = createElement('div', containerCSS, document.body, 'panel');

  container.textContent = '🚀';

  container.addEventListener('click', (e) => {
    dispatchIconClick(container);
  });

  container.addEventListener('mouseover', (e) => {
    // this create #panel--message
    displayMessageOnHover(container);
  })

  container.addEventListener('mouseout', (e) => {
    const messages = document.querySelectorAll('.panel--message');
    if(messages) {
      _.each(messages, function(node) {
        return node.remove()
      });
    }
  })

}

/*
 * targetNode where to append the message
 * the message
 * and how long it would display in milliseconds */
export function dropMessage (node, message, timeout) {
  if (!node)
    return console.log("dropMessage suppressed, node not available:", message);

  /* this function is called when the logos on the left have to dump some message,
     the id is the target where it should appear */
  const backup = node.innerHTML;
  const rect = node.getBoundingClientRect();
  node.innerHTML =
    '<span style="font-size:13px;width:400px;z-index:9999;background-color:#f0ddf0;margin:3px;left:' +
    (rect.left + 20) + ';bottom:' + (rect.bottom + 20) + ' ">' + message + '</span>';

  window.setTimeout(() => {
  /* when it is clicked, for the time out display a message, which initially was only
    "not an event page", in an event page, but it become more generic as a way to communicate */
    node.innerHTML = backup;
  }, timeout);

}
