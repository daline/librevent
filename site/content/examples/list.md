---
title: Typography & Examples
date: 2022-11-14
---

directly copied from the `exampleSite`:

1. [emoji-support](/examples/emoji-support)
2. [markdown-syntax](/examples/markdown-syntax)
3. [math-typesetting](/examples/math-typesetting)
4. [placeholder-text](/examples/placeholder-text)
5. [rich-content](/examples/rich-content)
