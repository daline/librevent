---
title: "#2nd Adversarial Interoperability Hackathon"
subtitle: "Berlin, c-base, June 2022"
date: 2022-09-22
draft: false

og_title: "Adversarial Interoperability Hackathon"
og_type: "website"
og_image: "https://libr.events/images/hckt-06-22/original-invite-mobe.png"
og_url: "https://libr.events/hackathon-june-2022"
og_description: "C-Base, Berlin, September 23rd 2022, on how to free events from the walled garden of Facebook"
---

Since version 0.2 of librevent we are moving to a more advanced design. In this hackathon we will address and discuss the limitations found in the previous model. We will discuss limitations (the use of our scripts is subordinated to having a fixed list of groups), technological (facebook's HTML is intentionally obfuscated to prevent any form of scraping) and usability, also because of the thoughts we had about the target audience.

<!--more-->

#### Resources / Links

* 20 minutes video recorded before the first hakathon: https://www.youtube.com/watch?v=deXHojuRXIc
* mobilizon.berlin announcement speech: https://blogs.fsfe.org/joseph/tag/mobilize-berlin/
* this hackathon event in mobilize.berlin: https://mobilize.berlin/events/9d80d85c-e198-4611-8e6d-edffe219db78
* librevent codebase: https://github.com/tracking-exposed/librevent
* mobilzone-reshare announcement and details https://write.as/simone-robutti/supercharge-your-events-using-mobilizon-and-mobilizon-reshare
* mobilzone-reshare code https://github.com/Tech-Workers-Coalition-Italia/mobilizon-reshare
* https://www.youtube.com/watch?v=6UOz6nrzntQ&t=3882s - video from https://digitalegesellschaft.de/2022/06/117-netzpolitischer-abend/
* c-base, Rungestraße 20 (near U8 Jannowitzbrücke / Heinrich-Heine-Strasse) https://www.openstreetmap.org/?mlat=52.51298&mlon=13.42010#map=19/52.51298/13.42010
* DISAPPEAR documentary https://www.arte.tv/en/videos/100750-000-F/disappear/

#### Version 0.2 of librevent

The mechanism below (developed by Tommy & Claudio), enables [advocate](/post/who-are-we-talking-to#3-the-advocates) to repost _semi-automatically_ events from a selected list of organizers.

{{< vimeo 744976039 >}}
_[librevent version 0.2 example](https://vimeo.com/744976039) from [Mobilize Berlin](https://mobilize.berlin)_.

The script fetch a list of events from the Organized page on Facebook. Then, it open each event with the mobile version of Facebook. Before reposting to mobilizon, it ask if the data have been extracted correctly. It repeats until there are not new events to liberate.


#### Media

<img src="/images/hckt-09-22/flyer-horizontal.png" />
