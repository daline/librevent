---
title: Software operation flow for version 0.3.x
date: 2022-12-25
draft: false
toc: true
---

This post has been written In preparation for the HIP talk ([7 slides first part](/pdf/2022-12-28-HIP-mobe.pdf), [17 slides second part](/pdf/2022-12-28-HIP-libr.pdf), [video on peertube](https://diode.zone/w/wy8nDtAQy7HRRtjdeJrLP7)), and contains a quite deep explanation on how the Librevents workflow connects: Facebook → Extension → Backend → Mobilizon.

In this post you'll also read about some current design limits in version 0.3.x, and the digital spaces where we can coordinate our efforts toward a stable 1.x release.

<!--more-->

## Software operation flow

{{<figure src="/images/flow/034-intro.jpeg" >}}

The first figure takeaways:

1. You should use the extension without being logged in
2. The Rocket icon only should work in Event page. Meaning the URL has to match this format `https://facebook.com/events/NUMERIC_EVENT_ID`, optionally with some `?parameter` after the `NUMERIC_EVENT_ID`, and those parameters are ignored. (for example: `https://www.facebook.com/events/456737733157167`).
3. The event need to be **expanded**. Facebook often cut the text and you've to press _See More_ to see the complete event. This need to be clicked before liberating the event.

{{<figure src="/images/flow/034-sending.jpeg" >}}

4. Once the event have been acquired, a few colored border would appear in the facebook page. It talks about the section captured and sent.
5. The backend is an intermediary step between the extension and the mobilizon instance. This is a design issue/feature that would likely to change in next version, but in this phase of development is necessary to pass through this approach (even if not privacy nor security wise)

{{<figure src="/images/flow/034-final.jpeg" >}}

6. This graph display the connections made between the `mobilizon-poster` library and the Mobilizon instance; Follow the [git repository](https://github.com/vecna/mobilizon-poster) to know more.

## Design limits

At this time, the extension must be aware of a backend server and have the necessary login and password to send the event to mobilizon. It means that a password is sent to a third party to do the authentication. This is not a clean or at the very least appropriate method, but there are reasons why I have come to this point:

7. mobilizon supports the [OAuth protocol](https://docs.joinmobilizon.org/administration/configure/auth/) but need to be configured at instance level, so not all the instances can be supported. It would be better to use that, but we haven't followed that implementation yet. It will happen in future versions, and is still unclear if the token exchange should be done either with libr.events website or from the extension. 
8. having a server that can keep track of freed IDs is useful to avoid duplication in the fediverse, although this should be solved with a DHT or other non-centralized technology.
9. at the moment the mobilizon interaction library is very sparse, it can barely do event updates, thinking about how to improve is part of the role separation between mobilizon support (which should be coupled with support for gath.io and hook) and the necessary registration one must have to interact with instances.

## Technical details on how the reposting works in the backend

This is a commented description on what happen when you run `npm run watch`. Assume you just clicked on the event with URL `https://www.facebook.com/events/4760265773991156`: 

{{<figure src="/images/mobi-poster/event-liberated.jpg" >}}

This happens in the backend, comments below the piece of logs:

<pre>
<font color="#268BD2"><b>lib:api </b></font> * Supporter papaya-kumquat-succotash [active on 22:59] last activity 22:58 26/12 (a minute ago) <font color="#268BD2">+1m</font>
<font color="#268BD2"><b>lib:api </b></font>Saved for debug /tmp/1203.json <font color="#268BD2">+1ms</font>
</pre>

The backend identify a `supporter` (someone with the extension installed) by their `publicKey`. The publicKey is normally not display in bytes, but with a pseudonym made of three random food words.

<pre>
<font color="#268BD2"><b>lib:api </b></font>Time travel problem! <font color="#268BD2">+2ms</font>
<font color="#268BD2"><b>lib:api </b></font>dissectDates from [SATURDAY, DECEMBER 31, 2022 AT 7:00 PM – 2:00 ] start: 2022-12-31 23:59 end 2022-12-31 23:59 <font color="#268BD2">+0ms</font>
</pre>

The dates might take different shape and formats. At the moment we didn't yet test all the possible conditions, but in cases like this (7pm on the last day of the year, till 2am of New Year Eve) and other complex combination, the software might fail in understand the meaning.

Every time the end date seems to be earlier than the start date, it is printed `Time travel problem!` and it is a bug.

<pre>
<font color="#D78700"><b>mobi:login </b></font>perform: Connecting to graphQL endpoint [https://mobilizon.libr.events/api] <font color="#D78700">+1m</font>
<font color="#D78700"><b>mobi:login </b></font>getIdentityInfo: Connecting to graphQL endpoint [https://mobilizon.libr.events/api] <font color="#D78700">+2s</font>
</pre>

`mobilizon-poster` connects to the mobilizon server, to get a valid authentication token. The token is necessary to post a new event.

<pre>
<font color="#B58900"><b>lib:core </b></font>Trying to resolve location in [ <font color="#859900">&apos;Market Grounds&apos;</font>, <font color="#859900">&apos;Pub&apos;</font>, <font color="#859900">&apos; &apos;</font>, <font color="#859900">&apos; · Restaurant&apos;</font> ] <font color="#B58900">+3s</font>
<font color="#5F00D7"><b>mobi:shared </b></font>Connecting to https://mobilizon.libr.events/api <font color="#5F00D7">+1m</font>
<font color="#00D787"><b>mobi:shared:IO </b></font>Status: 200 <font color="#00D787">+1m</font>
<font color="#875FD7"><b>mobi:location </b></font>Available 1 locations, selected [Market Grounds](115.855594;-31.949454) <font color="#875FD7">+1s</font>
<font color="#5F00D7"><b>mobi:shared </b></font>Connecting to https://mobilizon.libr.events/api <font color="#5F00D7">+1s</font>
<font color="#00D787"><b>mobi:shared:IO </b></font>Status: 200 <font color="#00D787">+814ms</font>
<font color="#875FD7"><b>mobi:location </b></font>Available 10 locations, selected [PUB](18.061288438256224;59.33456245) <font color="#875FD7">+815ms</font>
<font color="#5F00D7"><b>mobi:shared </b></font>Connecting to https://mobilizon.libr.events/api <font color="#5F00D7">+815ms</font>
<font color="#00D787"><b>mobi:shared:IO </b></font>Status: 200 <font color="#00D787">+726ms</font>
<font color="#B58900"><b>lib:core </b></font>Not found a location for [ ] <font color="#B58900">+3s</font>
<font color="#5F00D7"><b>mobi:shared </b></font>Connecting to https://mobilizon.libr.events/api <font color="#5F00D7">+726ms</font>
<font color="#00D787"><b>mobi:shared:IO </b></font>Status: 200 <font color="#00D787">+924ms</font>
<font color="#875FD7"><b>mobi:location </b></font>Available 10 locations, selected [Neubau (Metzgerei, Hotel, Restaurant, Tagungsräume) Hotel Wittmann](11.458678257358994;49.274438599999996) <font color="#875FD7">+923ms</font>
<font color="#B58900"><b>lib:core </b></font>Unusual condition where both of the label worked [ <font color="#859900">&apos;Market Grounds&apos;</font>, <font color="#859900">&apos;Pub&apos;</font>, <font color="#859900">&apos; &apos;</font>, <font color="#859900">&apos; · Restaurant&apos;</font> ] <font color="#B58900">+923ms</font>
</pre>

This piece of log need a few clarification:

1. Mobilizon expect a location resolved with a determined format, the same format of Open Street Map; every mobilizon instance wrap a query to OSM to so interactively offer the right spot
2. During the scraping you might acquire one or more possible location, this is a scraping problem, but this is the reason why three labels have been searched in this case.

<pre>
<font color="#AFD700"><b>mobi:poster </b></font>Successful event created: {
<font color="#AFD700"><b>mobi:poster </b></font>  __typename: <font color="#859900">&apos;Event&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  attributedTo: <b>null</b>,
<font color="#AFD700"><b>mobi:poster </b></font>  beginsOn: <font color="#859900">&apos;2022-12-31T23:59:59Z&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  category: <font color="#859900">&apos;MEETING&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  description: <font color="#859900">&apos;&lt;p&gt;Motif and Market Grounds presents...</font>[...]
<font color="#AFD700"><b>mobi:poster </b></font>  draft: <font color="#B58900">false</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  endsOn: <font color="#859900">&apos;2022-12-31T23:59:59Z&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  id: <font color="#859900">&apos;165&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  joinOptions: <font color="#859900">&apos;FREE&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  local: <font color="#B58900">true</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  onlineAddress: <font color="#859900">&apos;https://www.facebook.com/events/4760265773991156/&apos;</font>,
[...]
<font color="#AFD700"><b>mobi:poster </b></font>  title: <font color="#859900">&apos;The Night Market NYE 2023&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>  url: <font color="#859900">&apos;https://mobilizon.libr.events/events/df6791ef-f3e7-4234-92a9-c26ab8a8c6f1&apos;</font>,
<font color="#AFD700"><b>mobi:poster </b></font>} <font color="#AFD700">+2m</font>
<font color="#B58900"><b>lib:core </b></font>createEvent [The Night Market NYE 2023] successful! <font color="#B58900">+1s</font>
<font color="#268BD2"><b>lib:api </b></font>Written event post [SUCESSFUL] (https://mobilizon.libr.events/events/df6791ef-f3e7-4234-92a9-c26ab8a8c6f1) supporter papaya-kumquat-succotash, took a few seconds <font color="#268BD2">+10ms</font>
<font color="#2AA198"><b>librevent:server </b></font>API success, returning JSON &lt;31 bytes&gt; <font color="#2AA198">+0ms</font>
</pre>

The event has been created and inserted in the DB.

From the popup you can click "Access the Personal Page" where you can see the link of the event liberated, the mobilizon link just created, and few more information.

### ⚠️⚠️⚠️ In version 0.3.x the scraping/reposting bug can only be solved by editing the event in Mobilizon.

{{<figure src="/images/mobi-poster/personal-page-0.3.x.png" >}}

And below you can see how the event liberated looks like on mobilizon. The user `experiment` is the one provided by default in the browser extension.

{{<figure src="/images/mobi-poster/liberated-event-0.3.x.png" >}}

## Coordination spaces

We use the [Matrix Protocol](https://en.wikipedia.org/wiki/Matrix_(protocol)), to chat. And please don't propose a slack/mattermost solution, thank you.

[Use matrix](https://matrix.org/docs/projects/try-matrix-now/) and then connect to our chatroom: `#mobilize.berlin:systemli.org`.

**On the 15th day of every month, at 18:00 CET** we meet to chat and discuss last month progress and next month goal. This habit would be true for the `0.x` release and perhaps if the project proceed would be updated.

## Repository summary (extension and backend)

The command below have been tested on Ubuntu 20+ Linux. Also on MacOSX and Windows, potentially, this codebase can work. You might need to open the `buildall.sh` script and execute command line by line. It requires mongodb, nodejs, npm.

```
git clone https://0xacab.org/daline/librevents.git
cd librevents
./buildall.sh
```

If everything has worked, you can then:

1. Load the extension locally. The extension for `DEVELOPMENT` mode should be in `librevents/extension/build`. If you're new with this, it might help [Chrome install extension locally](https://developer.chrome.com/docs/extensions/mv2/getstarted/) or [Firefox install extension locally](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing). When the extension is loaded, please access a Facebook event at your choice and check if the rocket icon is appearing on the left.
2. from `librevents/backend` run `npm run watch` this would load the backend locally. the extension you just built would send data there, and because of the verbosity, you'll notice the activities.

## Compile the site, try to edit it, and potentially make a pull request

It requires [Hugo](https://gohugo.io/), as it is a static website generator, which is normally installed via `snap`, `flatpak`, and other package manager.
The repository also has a `submodule`, which is the [hugo-theme-texify](https://github.com/queensferryme/hugo-theme-texify.git).

```
git clone https://0xacab.org/daline/librevents.git
cd librevents/site
git submodule sync
hugo -D server
```

Then you can change the `librevents/site/content/**` files, and by accessing `http://localhost:1313`, see the website preview.
