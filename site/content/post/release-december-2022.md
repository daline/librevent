---
title: "The 0.3.4 release; bugfix and new iconography"
date: 2022-12-20
draft: false
---

This is the new extension icon: 🚀 What's better than a rocket to shoot free events in the Fediverse?

0.3.4 version for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/librevents/) and [Chrome](https://chrome.google.com/webstore/detail/librevents/kjklnndemgmhlogoigglkpkaklenommj). Or manually install the extension from the [git release](https://0xacab.org/daline/librevents/-/releases/v0.3.4).


<!--more-->

##### Fixes

* To get better insight, follow the list of the [closed issues](https://0xacab.org/daline/librevents/-/issues/?sort=created_date&state=closed&first_page_size=20).
* **fix**: updating settings via popup is correctly recorded. So to test the tool now you can use your login and password.
* **fix**: new iconography. The rocket appears on the right of the Facebook Event pages, and should be clicked.
* **fix**: typos and trimming in the popup, give a look on how it looks like:

{{<figure src="/images/popup-0.3.4.png">}}

<br />

##### What do the options mean?

* **Mobilizon**: this is the mobilizon server, you should add `/api` at the end, because what is needed is the API endpoint.
* **Login** and **Password**: login and password for the Mobilizon account.
* **Backend server**: keep `libr.events` as it is the only backend I know of, but if you run the [librevents backend](https://0xacab.org/daline/librevent) you can configure your own.
* 💡 Access the **Personal page**: this link brings you to the list of events you liberated, so you can review if they are correct and access to the Mobilizon entry where they can be updated if any detail was wrong.
