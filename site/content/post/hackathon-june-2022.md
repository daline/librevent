---
title: "#1st Adversarial Interoperability Hackathon"
subtitle: "Berlin, c-base, June 2022"
date: 2022-06-11
draft: false

---

This hackathon would be held at C-Base, Berlin. The goal is to explain the status of the project and collect potential supporters. Share a common vision, talk about component and trying to structure this work.
<!--more-->

### Help and goals

1. How quickly can be setup a Mobilizon instance? can it be tested and documented?
2. Test locally the extension and what the server is collecting - we are moving to a client side scraping
3. Do a unit test of the scraper by using the bunch of HTMLs we can collect
4. Improve statistics collection, find out meaningful metrics
5. Test the puppeteer method (which is not maintained, but it has worked for months)
6. Can the server automatically post in mobilizon? where the authentication credential should be handled?
7. Can the extension pick the picture?
8. Can you design a workflow for newcomers?
9. Can we review all the existing texts?

### Tweets/Tooth to send

This Saturday 11th of June, join [Mobilize Berlin](https://mobilize.berlin) and [trackingexposed](https://tracking.exposed) for a new hackathon at c-base from 14:00 to 20:00, and be part of our project called _librevent_. We want to take back control over how we socialize and get together. Let’s get free from digital platforms!

People have the right to experience their social and private life being free from invasive algorithms and greedy corporations that don’t share love for the communities they exploit. Personal data protection can be great, but we also need a safe space where to keep our events!

Improving our free-software tools we can make it easier to cross-post events on Mobilizon and other common event-management platforms. This can make life easier for those who want to organize and promote events without feeding the big tech-corps and surveillance capitalism

So take part to our hackathon at c-base this Saturday 11th of June from 14:00 to 20:00. Together we will learn how to use and improve our tool _librevent_! We will make our part for a more equal and decentralized world, imagining the future of our events and federated networks.


### Resources / Links

* mobilizon.berlin announcement speech: https://blogs.fsfe.org/joseph/tag/mobilize-berlin/
* this hackathon event in mobilize.berlin: https://mobilize.berlin/events/9d80d85c-e198-4611-8e6d-edffe219db78
* librevent codebase: https://github.com/tracking-exposed/librevent
* mobilzone-reshare announcement and details https://write.as/simone-robutti/supercharge-your-events-using-mobilizon-and-mobilizon-reshare
* mobilzone-reshare code https://github.com/Tech-Workers-Coalition-Italia/mobilizon-reshare
* https://www.youtube.com/watch?v=6UOz6nrzntQ&t=3882s - video from https://digitalegesellschaft.de/2022/06/117-netzpolitischer-abend/
* c-base, Rungestraße 20 (near U8 Jannowitzbrücke / Heinrich-Heine-Strasse) https://www.openstreetmap.org/?mlat=52.51298&mlon=13.42010#map=19/52.51298/13.42010
* DISAPPEAR documentary https://www.arte.tv/en/videos/100750-000-F/disappear/
