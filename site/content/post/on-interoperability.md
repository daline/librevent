---
title: "On interoperability"
date: 2020-12-26
draft: false
---

Interoperability is a property that frees us, gives us independence, and allows networks to grow by pandering to the needs of communities. It is not always a right, but this tool does not want to implement a law, it wants to enable Internet users to achieve interoperability regardless of what companies want, because that is how the Internet is supposed to work.

<!--more-->

## Interoperability is a characteristic of a system to work with other systems.

Librevents is a browser extension allowing any user to copy and republish (“scrape”) data about events posted on proprietary platforms onto free libre and open source decentralized networks. **It is an example of adversarial interoperability**, because wants to obtain the benefit knowing that this is against the monopolist intention and design.

There are two subject to keep in mind: *the monopolist* (Facebook, Eventbrite, Meetup...) and *the interoperable platform*. 

### Librevent would be a working beta when 1 monopolist and 1 interoperable platform are steadily supported.

Facebook event is the first natural target among the monopolist, and for the fediverse counterpart, Mobilizon will be the first platform supported. We look forward to integrate also in [gathio](https://gath.io) and [gancio](https://gancio.cisti.org/).

#### Know more

* An example: the Short Message Service (SMS) can be sent to everybody regardless of the provider they use or the brand of their phone. It may sounds simple and taken for granted, but is not. Interoperability is a right and we let as an exercise to the reader to see how this concept can apply in the internet of today, for example when it comes to Instant messages.
* [interoperability.news](https://interoperability.news).
