---
title: "30 minutes talk at HIP 2022"
date: 2022-12-07
draft: false
---

The talk proposed for HIP 2022 (Hacking in Parallel, the covid-safe replacement of the Chaos Computer Congress) [was accepted! and would happen on the 28th of December](https://pretalx.c3voc.de/hip-berlin-2022/schedule/).

<!--more-->

[TODO slides link](/TODO)

### Abstract 

Librevents is a browser extension allowing any user to copy and republish (“scrape”) data about events posted on proprietary platforms onto free libre and open source decentralized networks.

For now, Librevents focuses on liberating events data (description, date and time, location) from Facebook onto Mobilizon, an event-management platform alternative part of the Fediverse\*.

The intention behind Librevents is to feed alternative ethical platforms like Mobilizon with content, in order to help them counter the “network effect” (users staying on Facebook because the information is only available there). The concept of “data liberation” could later be applied to other types of contents and platforms.

The data we liberate is initially posted as a “public event” by the organizer. We make this data truly “public” and accessible on free platforms, without violating the organizer’s original intentions.

{{<figure src="/images/fediverse.png" >}}

\* [Fediverse](https://en.wikipedia.org/wiki/Fediverse): a federated network of free libre and open source alternatives to Twitter, Facebook, Youtube. The networks within the Fediverse communicate together through the [ActivityPub protocol](https://en.wikipedia.org/wiki/ActivityPub).

In this talk we will present the browser extension Librevents, ready to be used, and allowing any user to scrape and republish themselves events onto a Mobilizon instance.

Our goal is to grow our community of developers, event organizers, and Mobilizon instance administrators in order to improve Librevents, to promote Mobilizon and the Fediverse, and make data liberation happen.

#### keywords: Fediverse, Mobilizon data liberation, adversarial interoperability.
