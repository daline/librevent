---
title: "The 0.3.3 release and Lisbon Privacy Cafè"
date: 2022-11-18
draft: false

---

The upcoming presentation of Librevents in Lisbon, at the [Privacy Cafè](https://privacylx.org/), has pushed to deliver some of the progress foreseen at the last Hackathon. [Slides](/pdf/libr.events-0.3.x-PrivacyLX.pdf).

The 0.3.x release branch is the beginning of many parallel improvements! Get the browser extension for ([for Firefox](https://addons.mozilla.org/en-US/firefox/addon/librevents/), [for Chrome](https://chrome.google.com/webstore/detail/librevents/kjklnndemgmhlogoigglkpkaklenommj).)

<!--more-->

### What's new?

1. A new "conceptual space" hosts this software. The name is **Data Liberation Network** or _dalina_ for those in the family; Librevents was born under the  [Tracking Exposed Manifesto](https://tracking.exposed/manifesto) two years ago. Then, between 2021-2022 Tracking Exposed grew significantly, and was able to focus in algorithm analysis. It became natural, to highlight the different focus of Librevent, to move it to a more precise space.
2. There is a new dedicated [git repository](https://0xacab.org/daline/librevent) hosted on riseup.
3. The new extension has a calendar as an icon, and some scribbles that appear to the left of Facebook pages need to be clicked on.  The user experience is still problematic. We can aspire that by version 0.4 we will have [fixed most of these shortcomings](https://0xacab.org/daline/librevent/-/issues).
4. The [mobilezon-poster](https://www.npmjs.com/package/@_vecna/mobilizon-poster) has been updated to version 1.1.5.
5. This website still runs under hugo, like the previous one, but the LaTeX-inspired style gives it a whole other tone. Perhaps inappropriate with the revolutionary concept that data liberation might expect to be, which perhaps it is this dissonance to make this appealing.
6. As part of the stylistic experiment, the extension popup also has this font and has abandoned the psychedelic animated gif found at the bottom of the page.

### Commitment

We’ll have to build an international community of people who want to liberate data. The monopolistic social media model would keep frustrating us.

Of course, would have been more exciting if, for example, Twitter had gone out of business because people go on the fediverse, and not because some crazy billionaire drives you to there.

We would have all wanted communities to leave Facebook because it is a toxic and exploitative environment, and not because there are only bots, hate-filled boomers, and poorly targeted advertising.

If this is a phase of transition, **data liberation can be a bridge**, to help breaking the critical mass constraint, and leave those blazing dumpster once and for all.

#### References

The Privacy Cafè invite:

{{<figure
    src="https://privacylx.org/img/privacy-cafe-mill-nov2022.jpeg"
>}}

<br />
To not forget the thousands, if not the millions, of you lusting over an image morphing made by deep neural networks, mashed up to build a 90s-style banner for a web site.
<br />
<br />

{{<figure
    src="/images/libr.events.morphing.gif"
>}}

