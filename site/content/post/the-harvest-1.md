---
title: "The harvest 🍂 after January'23 planning"
date: 2023-02-10
draft: false
toc: true
description: "The primary goal is now to spread the message, and not to develop new features"
images: [ 'https://libr.events/images/libr.rocket.png' ]
---

🎉 Librevents has a [Mobilizon group](https://mobilize.berlin/@librevents). Check out the [🌱 Developer Meetup n.2 event announcement](https://mobilize.berlin/events/b234a22b-6fe2-4dc9-8394-75f8983b568c).
It would be in February 15th at 18:00 CET; Join the [Matrix channel](https://matrix.to/#/#mobilize.berlin:systemli.org) to introduce yourself and get a Jitsi link.

Read more to get a summary of the achievements after the [🌱 Developer Meetup n.1](/post/first-developer-meetup/).

<!--more-->

## Software and site updates

* Version 0.3.6 published - due to a critical bug detected immediately after the first series of announcements - as usual. The bug was discovered because the statistics were not showing any updates, so they actually worked ;)
* The software was contextually extended, [and so were the statistics](https://observablehq.com/@vecna/libr-events-usage-statistics), to see total activities, errors, and successfully released events. This should help to understand the overall stability.
* The pages [about](/about) and [who are we talking to](/post/who-are-we-talking-to) have been revised.
* Added an API to retrieve errors. Useful for debugging, but currently requires an admin key to access it because it may contain information we don't want to get out.
* Presented [librevents at OFFDEM](https://foss.events/2023/02-04-offdem.html), got fascinating contacts and a fully new context where to talk about it!

## Heard from the Fediverse


* Friendica visualizes events [in this very readable format](https://poliverso.org/display/84776f5b-d782-4beb-9a67-89ca1969250c) great for integration!
* We also get to know about [freeCodeCamp/chapter](https://github.com/freeCodeCamp/chapter) which is a software that handles events, but [does not seem to handle activitypub yet?](https://github.com/freeCodeCamp/chapter/issues/33).
* From that `issue` you can find this detailed analysis on _Event Sharing In The Fediverse_ at [socialhub.activitypub.rocks](https://socialhub.activitypub.rocks/t/federated-events/305/9), among them also:
* [Wordpress PHP ActivityPub support for events](https://github.com/pterotype-project/activitypub-php).


