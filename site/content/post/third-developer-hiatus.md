---
title: "⏸️ The project is temporarily on hiatus."
date: 2023-03-15
draft: false
description: "At the moment there is not the energy to develop the necessary progress, we need to strengthen the community to support it. There will be new updates when we have resolved this issue and can announce an updated roadmap."
images: [ 'https://libr.events/images/libr.rocket.png' ]
---

At the moment there is not the energy to develop the necessary progress, we need to strengthen the community to support it. _There will be new updates when we have resolved this issue and can announce an updated roadmap._

<!--more-->

Check the [contacts](/about/#contacts) section if you want get in touch.
