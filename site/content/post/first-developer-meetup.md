---
title: "🌱 Developer Meetup n.1; Report"
date: 2023-01-22
draft: false
toc: true
description: "The primary goal is now to spread the message, and not to develop new features"
images: [ 'https://libr.events/images/libr.rocket.png' ]
---

This is the report of the first librevent developer meeting; a hybrid solution between physical and digital meeting to discuss what to do between Jan. 15 and Feb. 15 at 6 p.m., the date of the second and next meeting.

<!--more-->

## Main outcomes 

The priority is to spread the [concept](/concept) (of both data liberation and librevents). To spread the extension that we have. Talk about it in the fediverse. Improve the explanation of what is going on, and measure how much, how, and whether this tool is appreciated and used.

**Therefore, if you** _more than once per year_ access `facebook.com` to look at an Event invitation, _even if unlogged because you don't have an account_, please consider installing:

{{<extension-button
  firefox="https://addons.mozilla.org/en-US/firefox/addon/librevents/"
  chrome="https://chrome.google.com/webstore/detail/librevents/kjklnndemgmhlogoigglkpkaklenommj" >}}

In the next month will be secondary to work on new features and new platforms (source and target), so **gath.io and eventbrite** are postponed.

Several “[gitlab issues](https://0xacab.org/daline/librevents/-/issues)” have been opened to keep track of the most anticipated improvements and the most trivial bugs. These are features that could help in the months ahead to make the data liberation process more pleasant, complete, and robust. But more prioritarian than improvements **we need to prioritize outreach and team consolidation**.

There should be no programmer-single-point-of-failure in development, and this condition should be addressed before making the tool more sophisticated and complex. Considering the code is has 6 years of baggage (being a heavily modified fork of `facebook.tracking.exposed`) and considering the looming migration to manifest V3, rewriting the extension and reorganizing the backend is a refactor to consider.

## Goals for the next meeting

**Raise awareness about the project**. We should find early adopters, developers, and moral support. In support of this we should spread the current produced material and produce new media.

## The talk gave at HIP'22 [is now on peertube](https://diode.zone/w/wy8nDtAQy7HRRtjdeJrLP7)

<iframe title="Librevent presentation at HIP 2022" src="https://diode.zone/videos/embed/f76e811c-831b-481e-a8c6-a7b24f5b2184" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="800" height="640" frameborder="0"></iframe>

Slides: [7 slides (first part)](/pdf/2022-12-28-HIP-mobe.pdf), [17 slides (second part)](/pdf/2022-12-28-HIP-libr.pdf).

## How to measure progress 

Since one way to measure success is to know how many people are using it, an important thing is to know how many users there are, so statistics must be prioritized. So an API (`https://libr.events/api/stats`) has been implemented to export aggregate counters.

#### 👉 [data is consumed and visualized in this _observable_](https://observablehq.com/@vecna/libr-events-usage-statistics).

## Minor bugfix

The [incorrect text strings at the end of text](https://0xacab.org/daline/librevents/-/issues/6#note_501003) has been fixed.
