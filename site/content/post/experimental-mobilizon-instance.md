---
title: "A place for our experiments: mobilizon.libr.events"
date: 2022-09-18
draft: false
---

We created a mobilizon instance to experiment. It can be used by anyone, but you should expect a lot of chaos there. 

<!--more-->

Check out the instance at:

## [mobilizon.libr.events](https://mobilizon.libr.events)

Note, librevent is not intended to be an _automated_ scraping system but a scraping system that is used manually. This instance is also used to test mobilizon-poster, so you may notice the presence of artificially created events. Here the [Instance configuration](https://mobilizon.libr.events/about/instance).
