---
title: About Librevents
description: "Software, politics and background of one of the first Data Liberation Projects"
images: [ 'https://libr.events/images/libr.rocket.png' ]

date: 2021-05-07
layout: page
draft: false
---

### What constitutes this project?

A couple of software written in AGPL-3 license:

* [_librevents_](https://0xacab.org/daline/librevents/), a fork of [facebook.tracking.exposed](https://facebook.tracking.exposed), the toolkit for Facebook algorithm analysis.
* [_mobilizon-poster_](https://github.com/vecna/mobilizon-poster) a nodejs package to interact with mobilizon.

##### Who is this project made for? 👉 [audience and adopters analysis](/post/who-are-we-talking-to/).

<br>

---

<br>

### Contacts

* [Librevents updates](https://mobilize.berlin/@librevents), a Mobilizon group to follow us in the Fediverse.
* [Matrix channel](https://matrix.to/#/#mobilize.berlin:systemli.org).
* Mail `daline at libr dot events` (very rarely checked).

<br>

### Backstory, or, where this come from?

* The [Tracking Exposed manifesto](https://tracking.exposed/manifesto).
* The will to support a journalistic/artistic product aimed at talking about your digital rights, the [DISAPPEAR](https://www.arte.tv/en/videos/100750-000-F/disappear/) documentary (sequel of [Nothing to Hide](https://vimeo.com/189016018)).
* The need to explore new ways to counter surveillance capitalism.
* Finally, **current democratic institutions do not regulate the interoperability of social media**. The only reason they have not yet done so is because of the enormous lobbying pressure, which we believe is a very unfair and unbalanced way of influencing regulations in our society. So we have to move forward with the tools that we have left and that we control.

<br>

---

<br>

### Complementary projects connected to this

* [_mobilize.berlin_](https://mobilize.berlin/about/instance) the mobilizon instance where this tool has been developed for.
* [_mobilizon-reshare_](https://github.com/Tech-Workers-Coalition-Italia/mobilizon-reshare) a python toolkit to repost material shared on mobilzon.
* [_Tracking Exposed_](https://tracking.exposed) expose influential algorithms, develop technology and analysis methodologies, to increase knowledge of the problem that big tech is.

