---
title: "Librevents: “liberating” data from Big Tech"
date: 2019-08-09:01:21+01:00
draft: false
description: "Librevents is a browser extension allowing any user to copy and republish (“scrape”) data about events posted on proprietary platforms onto free libre and open source decentralized networks."
---
