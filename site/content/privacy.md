---
title: How data is processed in libr.events
date: 2022-08-01
layout: page
draft: false
---

### How data is processed in libr.events

A.K.A. *the privacy policy*

### Preliminary statement

This is an experiment, a work in progress that might break tomorrow and we might not even notice it. We can't assure you this tool would actually do what you think might do. The code is free software, AGPL-3 license, and can be scrutinzed or receive contributions by anyone. Please feel free to suggest improvement or help us in the development process.

Let's assume you decide to use librevents, there are a few things to know:

0. In the browser extension there is a pop-up. There you can configured a `backend` server, by default is `https://libr.events` which is our server. Our server has not backups, has only one admin dedicated to run experiments, and should not be seen as a stable place. From that interface you can **decide which server to use**, and we invite everyone to run their own server.

In the case you are keeping the default settings, please read the following privacy statement:

### Do we process personal data?

Our main goal is to process public events, therefore the answer **should be NO**, altough, there are few technical details (data processing for technical reason) to address here.

1. We store the information you write in the extension pop-up. Among them, the login and password of Mobilizon. We know this is not OK, and a safer solution would be found before releasing a stable version, but in this experimental phase we can't do yet anything more complex.
2. We store the data relative to the `Events` you want to repost to mobilizon, so you can review and we can avoid to let you re-post the same event twice.

### Concept and scope of the data processing

3. You're in control of your data, by using the browser extension you can access what we call `personal page` and that allow you control. Such as: review which events you re-posted, and if the re-post has worked or not. Link to the original event and to the mobilizon copy. You can access to this `personal page` because the URL contains a secret, generated automatically by your browser extension. Don't make it public. If you lose control of your access token, becuause for example you share it by mistake, you can uninstall and reinstall the browser extension, it would re-create a new access token. With only the access token a person can't see your login and password (that's only possible from the browser extension).
4. We store events you liberate, from any `facebook.com/events` page. Every event you sent us is automatically **deleted from our server after 30 days**.
5. We repost them automatically by using the login and password you provide into the browser extention popup.

### Contacts

In any case you want to get an update, delete, ask more on how your data is processed, you can write to `privacy at libr dot events`.

