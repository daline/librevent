---
title: Private activity log
date: 2022-11-18
draft: false
---

## [🏠](/) Private activity log

---

<br>

#### Your profile

<table>
  <thead>
    <tr>
      <th>First access</th>
      <th>Last access</th>
      <th>Events liberated</th>
      <th>Errors</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td id="first--access"></td>
      <td id="last--access"></td>
      <td id="events--liberated"></td>
      <td id="errors"></td>
    </tr>
  </tbody>
</table>

#### Event you liberated

<table>
  <thead>
    <tr>
      <th>When</th>
      <th>Title</th>
      <th>Event Time</th>
      <th>Links</th>
    </tr>
  </thead>
  <tbody id="event--list">
    <!-- 
      here a bunch of <td> gets injected by personal.js,
      wrapped in a <tr></tr> 
    -->
  </tbody>
</table>

_Discuss improvements in the [git repository](https://0xacab.org/daline/librevent/-/issues)_.

<br />
<br />
<br />

---

<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/lodash.min.js"></script>
<script src="/js/global.js"></script>
<script src="/js/personal.js"></script>
<script type="text/javascript">
    /* personal() is implemtended in personal.js and fetch remotely the support and events data */
    $("#header").hide();
    $( document ).ready(function() {
        personal();
    });
</script>
